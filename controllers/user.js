const User = require('../models/user')

exports.postCreateUser = (req, res, next) => {
    let { firstName, lastName, email } = req.body
    User.create({
        firstName,
        lastName,
        email
    }).then((user) => {
        res.status(200).json(user)
    }).catch((err) => {
        res.status(500).send('User not created')
        console.log(err.message)
    })
}

exports.getUsers = (req, res, next) => {
    User.findAll()
        .then((users) => {
            res.status(200).json(users)
        }).catch((err) => {
            console.log(err.message)
        })
}

exports.getUser = (req, res, next) => {
    let { id } = req.params
    User.findByPk(id)
        .then((user) => {
            if (!user) {
                res.status(404).send('User not found.')
            }
            res.status(200).json(user)
        }).catch((err) => {
            console.log(err.message)
        })
}
