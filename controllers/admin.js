const Product = require('../models/product')

exports.getProducts = (req, res, next) => {
    Product.findAll()
        .then((products) => {
            if (products.length > 0) {
                res.json(products)
            } else {
                res.send('No products yet...')
            }
        })
        .catch((err) => {
            console.log(err)
        })
}

exports.getProduct = (req, res, next) => {
    let id = req.params.id

    Product.findByPk(id)
        .then((product) => {
            if (!product) {
                return res.status(404).send('Product not found.')
            }
            res.json(product)
        })
        .catch((err) => {
            console.log(err.message)
        })
}


exports.postAddProduct = (req, res, next) => {
    let name = req.body.name
    let price = req.body.price
    let description = req.body.description
    let imageUrl = req.body.imageUrl

    Product.create({
        name,
        price,
        description,
        imageUrl
    })
        .then((product) => {
            res.status(200).json(product)
        })
        .catch((err) => {
            console.log(err.message)
        })
}

exports.postDeleteProduct = (req, res, next) => {
    let { id } = req.params

    Product.destroy({ where: { id } })
        .then((product) => {
            res.status(200).send('A product has been deleted.')
        })
        .catch((err) => {
            console.log(err.message)
        })
}

exports.postUpdateProduct = (req, res, next) => {
    let { id } = req.params
    let { name, price, description, imageUrl } = req.body

    Product.findByPk(id)
        .then((product) => {
            product.name = name
            product.price = price
            product.description = description
            product.imageUrl = imageUrl
            product.save()
            res.status(200).json(product)
        })
        .catch((err) => {
            console.log(err.message)
        })
}
