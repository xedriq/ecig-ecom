const Product = require('../models/product')

exports.getProducts = (req, res, next) => {
    Product.findAll()
        .then((products) => {
            if (products.length > 0) {
                res.json(products)
            } else {
                res.send('No products yet...')
            }
        })
        .catch((err) => {
            console.log(err)
        })
}

exports.postCart = (req, res, next) => {
    let { productId } = req.body
    let fetchCart
    let newQuantity = 1

    req.user.getCart()
        .then((cart) => {
            if (!cart) {
                return req.user.createCart()
            }
            return cart
        })
        .then((cart) => {
            fetchCart = cart
            return cart.getProducts({ where: { id: productId } })
        })
        .then((products) => {
            let product

            //checks if the product being added is on the cart
            if (products.length > 0) {
                product = products[0]
            }

            if (product) {
                const prevQuantity = product.cartProduct.quantity
                newQuantity = prevQuantity + 1
                return product
            }

            return Product.findByPk(productId)
        })
        .then((product) => {
            return fetchCart.addProduct(product, { through: { quantity: newQuantity } })
        })
        .then(() => {
            fetchCart.getProducts()
                .then(products => { res.json(products) })
                .catch(err => { console.log(err.message) })
        })
        .catch((err) => {
            console.log(err.message)
        })
}

exports.getCart = (req, res, next) => {
    req.user.getCart()
        .then(cart => {
            cart.getProducts()
                .then(products => {
                    res.json(products)
                }).catch(err => { console.log(err.message) })
        }).catch(err => { console.log(err.message) })
}

