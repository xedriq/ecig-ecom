const { Sequelize, DataTypes } = require('sequelize')

const sequelize = require('../util/db')

const Product = sequelize.define('product', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
            isDecimal: true
        }
    },
    imageUrl: DataTypes.STRING,
    description: DataTypes.STRING,
})

module.exports = Product