const express = require('express')

const router = express.Router()

const jsonParser = express.json()

//controllers
const adminController = require('../controllers/admin')
const userController = require('../controllers/user')

// product routes
router.get('/products', adminController.getProducts)
router.get('/products/:id', adminController.getProduct)
router.post('/add-product', jsonParser, adminController.postAddProduct)
router.delete('/delete-product/:id', adminController.postDeleteProduct)
router.patch('/update-product/:id', adminController.postUpdateProduct)

//user routes
router.get('/users', userController.getUsers)

module.exports = router