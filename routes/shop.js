const express = require('express')

const router = express.Router()
const jsonParser = express.json()

const shopController = require('../controllers/shop')

router.get('/', shopController.getProducts)

router.post('/cart', jsonParser, shopController.postCart)
router.get('/cart', shopController.getCart)

module.exports = router