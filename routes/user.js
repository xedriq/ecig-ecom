const express = require('express')
const bodyParser = require('body-parser')

const router = express.Router()

const userController = require('../controllers/user')

const jsonParser = bodyParser.json()

router.post('/user/register', jsonParser, userController.postCreateUser)

router.get('/users/:id', userController.getUser)

module.exports = router