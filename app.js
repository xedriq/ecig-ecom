const express = require('express')
const path = require('path')

const port = 3000

// db connection
const sequelize = require('./util/db')

//import routes
const adminRouter = require('./routes/admin')
const shopRouter = require('./routes/shop')
const errorRouter = require('./routes/error')
const userRouter = require('./routes/user')

// db imports
const User = require('./models/user')
const Product = require('./models/product')
const Cart = require('./models/cart')
const CartProduct = require('./models/cart-product')

// db relations
User.hasOne(Cart)
Product.belongsToMany(Cart, { through: CartProduct })
Cart.belongsToMany(Product, { through: CartProduct })

const app = express()

// serve static folder
app.use(express.static(path.join(__dirname, 'public')))

app.use(express.urlencoded({ extended: false }))

// set user with id 1 as logged-in user
app.use((req, res, next) => {
    User.findByPk(1)
        .then((user) => {
            req.user = user
            next()
        }).catch((err) => {
            console.log(err.message)
        })
})

// use routes
app.use('/admin', adminRouter)
app.use(userRouter)
app.use(shopRouter)
app.use(errorRouter)

sequelize
    // .sync({ force: true })
    .sync()
    .then((result) => {
        console.log('DB synched successfully...')
        app.listen(port, () => {
            console.log(`Server running on port ${port}...`)
        })
    })
    .catch((err) => {
        console.log(err)
    })
